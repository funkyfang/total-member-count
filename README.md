# Total Member Count

This script totals the number of members in groups that you admin.

_It doesn't total the unique members._ If a person is in two groups, that person is counted twice.

## 1. Before running the script,
- Get your user access token from an app that you admin from the [Graph API Explorer](https://developers.facebook.com/tools/explorer/). You must grant one of the [Groups API permissions](https://developers.facebook.com/docs/groups-api/#requirements).
- Confirm that your an admin of the groups that you'd like to count.

## 2. To run the script,
-  Download the file into a directory.
-  From that directory, run the script using the access token from #1: ```php total_member_count.php {YOUR-ACCESS-TOKEN}```. 