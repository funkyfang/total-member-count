<?php
// For script instructions, visit:
// https://bitbucket.org/funkyfang/total-member-count/src/master/README.md

$access_token = $argv[1];

$graph_api = "https://graph.facebook.com";

$groups = [];
$response = @file_get_contents("$graph_api/me/groups?access_token=$access_token");
if ($response === FALSE) {
  print "Error: Try getting a new user access token from https://developers.facebook.com/tools/explorer/. If that doesn't work, try again in an hour.\n";
} else {
  $response = json_decode($response, true);
  $groups = $response['data'];
  while (!empty($response['paging']) && !empty($response['paging']['next'])) {
    $response = file_get_contents($response['paging']['next']);
    if (!empty($response['data'])) {
      array_merge($groups, $response['data']);
    }
  }
}

if ($response !== FALSE) {
  $count = 0;
  foreach ($groups as $group) {
    $group_id = $group['id'];
    $response = @file_get_contents("$graph_api/$group_id?fields=member_count&access_token=$access_token");
    if ($response === FALSE) {
      print "Error: Check if you're an admin of group id $group_id\n";
    } else {
      $response = json_decode($response, true);
      $member_count = $response['member_count'];
      print "Member count for group id $group_id is $member_count\n";
      $count += $member_count;
    }
  }
  print "\nTOTAL: $count\n";
}
